<?php

namespace drew\youtubeuploader;

use Google_Client;
use Google_Service_Exception;
use Google_Http_MediaFileUpload;
use Google_Service_YouTube;
use Google_Service_YouTube_Video;
use Google_Service_YouTube_VideoSnippet;
use Google_Service_YouTube_VideoStatus;

/**
 * Description
 */
class YoutubeUploader
{
    public static $chuckSizeBytes = 1024 * 1024;

    /**
     * @var Google_Client
     */
    protected $googleClient;

    /**
     * @var YoutubeUploadableInterface
     */
    protected $uploadableFile;

    public function __construct(Google_Client $googleClient)
    {
        $this->googleClient = $googleClient;
    }

    public function upload(YoutubeUploadableInterface $uploadableFile): string
    {
        $this->uploadableFile = $uploadableFile;
        try {
            $id = $this->doUpload();
        } catch (Google_Service_Exception $e) {
            print_r($e->getErrors());
            print_r([
                'title' => $uploadableFile->getTitle(),
                'descr' => $uploadableFile->getDescription(),
            ]);
            throw new \Exception($e->getMessage());
        }
        return $id;
    }

    protected function doUpload(): string
    {
        $this->googleClient->setDefer(true);
        $media = $this->buildMediaFileUpload();
        // Read the media file and upload it chunk by chunk.
        $status = false;
        $handle = fopen($this->uploadableFile->getFilepath(), 'rb');
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, static::$chuckSizeBytes);
            $status = $media->nextChunk($chunk);
        }
        fclose($handle);
        $this->googleClient->setDefer(false);
        return $status['id'];
    }

    protected function buildMediaFileUpload()
    {
        $youtube = new Google_Service_YouTube($this->googleClient);
        $mediaFileUpload = new Google_Http_MediaFileUpload(
            $this->googleClient,
            $youtube->videos->insert('status,snippet', $this->buildVideo()),
            'video/*',
            null,
            true,
            static::$chuckSizeBytes
        );
        $fileSize = filesize($this->uploadableFile->getFilepath());
        $mediaFileUpload->setFileSize($fileSize);
        return $mediaFileUpload;
    }

    protected function buildVideo()
    {
        $video = new Google_Service_YouTube_Video();
        $video->setSnippet($this->buildVideoSnippet());
        $video->setStatus($this->buildVideoStatus());
        return $video;
    }

    protected function buildVideoSnippet()
    {
        $videoSnippet = new Google_Service_YouTube_VideoSnippet();
        $videoSnippet->setTitle($this->uploadableFile->getTitle());
        $videoSnippet->setDescription($this->uploadableFile->getDescription());
        return $videoSnippet;
    }

    protected function buildVideoStatus()
    {
        $videoStatus = new Google_Service_YouTube_VideoStatus();
        $videoStatus->setPrivacyStatus($this->uploadableFile->getPrivacyStatus());
        return $videoStatus;
    }
}
