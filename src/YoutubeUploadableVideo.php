<?php

namespace drew\youtubeuploader;

/**
 * Description
 */
class YoutubeUploadableVideo implements YoutubeUploadableInterface
{
    const PRIVACY_PUBLIC = 'public';
    const PRIVACY_UNLISTED = 'unlisted';
    const PRIVACY_PRIVATE = 'private';

    protected $filepath;
    protected $title = 'default title';
    protected $description = 'default description';
    protected $privacyStatus;

    public function __construct(string $filepath)
    {
        if (!$this->filepath = realpath($filepath)) {
            throw new \Exception(sprintf('File "%s" does not exist', $filepath));
        }
    }

    public function getFilepath(): string
    {
        return $this->filepath;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): string
    {
        return mb_substr($this->description, 0, 10000, 'utf-8');
    }

    public function setPrivacyStatus(string $status)
    {
        $this->privacyStatus = $status;
        return $this;
    }

    public function getPrivacyStatus(): string
    {
        return $this->privacyStatus ?: static::PRIVACY_PUBLIC;
    }
}
