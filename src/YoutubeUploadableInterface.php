<?php

namespace drew\youtubeuploader;

interface YoutubeUploadableInterface
{

    public function getFilepath(): string;

    public function getTitle(): string;

    public function getDescription(): string;

    public function getPrivacyStatus(): string;
}
